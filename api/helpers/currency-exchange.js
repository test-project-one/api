let fx = require('money')
let oxr = require('open-exchange-rates')
oxr.set({ app_id: sails.config.custom.oxrAppID })

module.exports = {

  friendlyName: 'Currency exchange',
  description: 'Currency exchange functionality (integration with data from Open Exchange Rates API).',

  inputs: {
    date: {
      description: 'The date for check.',
      type: 'string',
      required: true
    },
    value: {
      description: 'Value for exchange from one currency to another.',
      type: 'number',
      required: true
    },
    from: {
      description: 'Code of currency.',
      type: 'string',
      required: true
    },
    to: {
      description: 'Code of currency.',
      type: 'string',
      required: true
    }
  },

  exits: {},

  async fn (inputs, exits) {
    // Get exchange rates for selected currencies.
    oxr.historical(inputs.date, (err) => {
      if (err) {
        sails.log.debug(new Date().toISOString(), 'ERROR loading data from Open Exchange Rates API! Error was: ', err.toString())
        // todo: need to check fall back!
        return exits.error('Error of loading data from Open Exchange Rates API!')
      }
      sails.log.debug(new Date().toISOString(), 'Successfully loaded data from Open Exchange Rates API!')

      // Throw error, if one of the selected exchange rate is not exist.
      if (!oxr.rates[inputs.from] || !oxr.rates[inputs.to]) {
        return exits.error(`One of the selected exchange rate is not exist in Rates API for date ${inputs.date}!`)
      }

      // We load rates into the money.js (fx) library for easier currency conversion.
      fx.rates = oxr.rates
      fx.base = oxr.base
      return exits.success(fx(inputs.value).from(inputs.from).to(inputs.to).toFixed(2))
    })
  }

}
