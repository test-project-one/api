var blockexplorer = require('blockchain.info/blockexplorer')

module.exports = {

  friendlyName: 'Get latest blocks',
  description: 'Get latest blocks from https://blockchain.info.',

  inputs: {
    itemsPerPage: {
      description: 'Count of items per page (pagination).',
      type: 'number',
      required: true
    },
    page: {
      description: 'Number of the current page (pagination).',
      type: 'number',
      required: true
    },
    sortBy: {
      description: 'The name of field for the sorting (pagination).',
      type: ['string'],
      defaultsTo: ['time']
    },
    sortDesc: {
      description: 'The name of field for the sorting (pagination).',
      type: ['boolean'],
      defaultsTo: []
    }
  },

  exits: {
    invalid: {
      responseType: 'badRequest',
      description: 'Exchange error!'
    }
  },

  async fn (inputs, exits) {
    const response = await blockexplorer.getBlocks('').catch(() => { throw 'invalid'})

    return exits.success({
      data: _.sortByOrder(response.blocks, [inputs.sortBy.pop()], [inputs.sortDesc.pop() ? 'desc' : 'asc']).slice((inputs.page - 1) * inputs.itemsPerPage, inputs.page * inputs.itemsPerPage),
      totalItems: response.blocks.length
    })
  }

}
