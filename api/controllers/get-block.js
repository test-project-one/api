var blockexplorer = require('blockchain.info/blockexplorer')

module.exports = {

  friendlyName: 'Get block details',
  description: 'Get block details by hash from https://blockchain.info.',

  inputs: {
    hash: {
      description: 'The hash of blockchain block.',
      type: 'string',
      required: true
    }
  },

  exits: {
    invalid: {
      responseType: 'badRequest',
      description: 'Exchange error!'
    }
  },

  async fn (inputs, exits) {
    const response = await blockexplorer.getBlock(inputs.hash).catch(() => { throw 'invalid'})

    let transactions = []
    for (let tx of response.tx) {
      transactions.push(
        {
          index: tx.tx_index,
          hash: tx.hash,
          size: tx.size
        }
      )
    }

    return exits.success({
      hash: response.hash,
      size: response.size,
      block_index: response.block_index,
      prev_block: response.prev_block,
      fee: response.fee,
      tx_count: response.tx.length,
      tx: transactions
    })
  }

}
