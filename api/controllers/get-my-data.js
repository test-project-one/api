module.exports = {

  friendlyName: 'Get data',
  description: 'Get some data (test route).',

  inputs: {
    date: {
      description: 'The date for check.',
      type: 'string',
      required: true
    },
    value: {
      description: 'The value for calculate.',
      type: 'number',
      required: true
    }
  },

  exits: {
    invalid: {
      responseType: 'badRequest',
      description: 'Exchange error!'
    }
  },

  async fn (inputs, exits) {
    // Exchange USD to BTC for selected date.
    let value = await sails.helpers.currencyExchange.with({
      date: inputs.date,
      value: inputs.value,
      from: 'USD',
      to: 'BTC'
    }).intercept(() => { return 'invalid' })

    // Exchange BTC to USD for current date.
    value = await sails.helpers.currencyExchange.with({
      date: new Date().toISOString().slice(0, 10),
      value: value,
      from: 'BTC',
      to: 'USD'
    }).intercept(() => { return 'invalid' })

    return exits.success(value)
  }

}
